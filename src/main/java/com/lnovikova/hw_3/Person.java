package main.java.com.lnovikova.hw_3;

public class Person {
    private int xCoordinate;
    private int yCoordinate;
    private char name;
    private Hero heroName;

    public Person(int xCoord, int yCoord, Hero hero, char name) {
        xCoordinate = xCoord;
        yCoordinate = yCoord;
        heroName = hero;
        this.name = name;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public Hero getHeroName() {
        return heroName;
    }

    public void setHeroName(Hero heroName) {
        this.heroName = heroName;
    }
}
