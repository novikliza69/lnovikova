package main.java.com.lnovikova.hw_3;

public class hw_3 {
    public static void main(String[] args) {
        Person princess = new Person(0, 9, Hero.Princess, 'G');
        Person prince = new Person(9, 0, Hero.Prince, 'P');
        Game game = new Game(10);
        game.fillGameArea();
        for (int i = 0; i < 6; i++) {
            game.addHeroToGameArea(new Person(random(), random(), Hero.Monster, 'M'));
        }
        game.addHeroToGameArea(princess);
        game.addHeroToGameArea(prince);
        printArray(game.getGameArea());
        while (game.getDistanceBetweenPrinceAndPrincess(princess, prince) != true) {
            game.uploadGameArea(prince);
            printArray(game.getGameArea());
        }
        if (game.getDistanceBetweenPrinceAndPrincess(princess, prince)) {
            System.out.print("Принцесса спасена!!!");
            System.out.print("Количество шагов:" + game.getCount());
        }
    }

    static int random() {
        return 1 + (int) (Math.random() * 8);
    }

    static void printArray(char[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
    }
}
