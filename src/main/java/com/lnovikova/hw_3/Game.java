package main.java.com.lnovikova.hw_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {
    private char[][] gameArea;
    private int count;

    public Game(int n) {
        gameArea = new char[n][n];
    }

    public void fillGameArea() {
        for (int i = 0; i < gameArea[0].length; i++) {
            for (int j = 0; j < gameArea[1].length; j++) {
                gameArea[i][j] = '.';
            }
        }
    }

    public void addHeroToGameArea(Person hero) {
        gameArea[hero.getxCoordinate()][hero.getyCoordinate()] = hero.getName();
    }

    private int getDirection() {
        int direction = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            direction = Integer.parseInt(br.readLine());
        } catch (IOException ex) {
            System.out.println("Ошибка ввода");
        } catch (NumberFormatException ex) {
            System.out.println("Неправильный формат значения");
        }
        return direction;
    }

    public void uploadGameArea(Person prince) {
        if (prince.getHeroName() == Hero.Prince) {
            gameArea[prince.getxCoordinate()][prince.getyCoordinate()] = '.';
            moveHero(prince);
            gameArea[prince.getxCoordinate()][prince.getyCoordinate()] = prince.getName();
        }
    }

    public void moveHero(Person prince) {
        int direction = getDirection();
        if (direction == 8) {
            if (prince.getxCoordinate() == 0) {
                prince.setxCoordinate(prince.getxCoordinate());
            } else if (gameArea[prince.getxCoordinate() - 1][prince.getyCoordinate()] == 'M') {
                prince.setxCoordinate(prince.getxCoordinate());
            } else {
                prince.setxCoordinate(prince.getxCoordinate() - 1);
                count++;
            }
        } else if (direction == 2) {
            if (prince.getxCoordinate() == 9) {
                prince.setxCoordinate(prince.getxCoordinate());
            } else if (gameArea[prince.getxCoordinate() + 1][prince.getyCoordinate()] == 'M') {
                prince.setxCoordinate(prince.getxCoordinate());
            } else {
                prince.setxCoordinate(prince.getxCoordinate() + 1);
                count++;
            }
        } else if (direction == 6) {
            if (prince.getyCoordinate() == 9) {
                prince.setyCoordinate(prince.getyCoordinate());
            } else if (gameArea[prince.getxCoordinate()][prince.getyCoordinate() + 1] == 'M') {
                prince.setyCoordinate(prince.getyCoordinate());
            } else {
                prince.setyCoordinate(prince.getyCoordinate() + 1);
                count++;
            }
        } else if (direction == 4) {
            if (prince.getyCoordinate() == 0) {
                prince.setyCoordinate(prince.getyCoordinate());
            } else if (gameArea[prince.getxCoordinate()][prince.getyCoordinate() - 1] == 'M') {
                prince.setyCoordinate(prince.getyCoordinate());
            } else {
                prince.setyCoordinate(prince.getyCoordinate() - 1);
                count++;
            }
        }
    }

    public boolean getDistanceBetweenPrinceAndPrincess(Person princess, Person prince) {
        boolean findPrincess = false;
        if (princess.getxCoordinate() - prince.getxCoordinate() == 0 && princess.getyCoordinate() - prince.getyCoordinate() == 0)
            findPrincess = true;
        return findPrincess;
    }

    public char[][] getGameArea() {
        return gameArea;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
