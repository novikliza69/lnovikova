package com.lnovikova.hw_8;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class OrderServlet extends HttpServlet {

    //List items=new ArrayList();
    //HttpSession session;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session=req.getSession();
        List orders=(List)session.getAttribute("section");
        if (orders == null) {
            orders = new ArrayList<>();
            session.setAttribute("section", orders);
        }
        String item = (String)req.getParameter("section");
        String action=req.getParameter("action");
        if("Add Item".equalsIgnoreCase(action)){
            orders.add(item);
            resp.sendRedirect("order.jsp");
        }
        else if("Submit".equalsIgnoreCase(action)){
            double sum=0;
            for(Object i:orders){
                sum+=Double.valueOf(i.toString().replaceAll("[^0-9.]",""));
            }
           session.setAttribute("sum",sum);
           resp.sendRedirect("finish.jsp");
        }
    }
}
