package main.java.com.lnovikova.hw_5;

import java.util.*;

public class hw_5 {
    public static void main(String[] args) {
        String sentence = "Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but a\n" +
                "Lamb just beginning to drink a little lower down.";
        String[] words = splitIntoWords(sentence);
        String[] wordsWithoutDuplicates = removeDuplicates(words);
        Map<Character, ArrayList<String>> groupWords = groupWordsByAlphabet(wordsWithoutDuplicates);
        for (Map.Entry entry : groupWords.entrySet()) {
            System.out.println(entry.getKey() + ": "
                    + entry.getValue().toString());
        }

    }

    public static String[] splitIntoWords(String sentence) {
        String[] words = sentence.toLowerCase().split("[,;:.!?\\s]+");
        return words;
    }

    public static String[] removeDuplicates(String[] array) {
        Set<String> set = new HashSet<String>(Arrays.asList(array));
        String[] arrayWithoutDuplicates = set.toArray(new String[set.size()]);
        return arrayWithoutDuplicates;
    }

    public static Map<Character, ArrayList<String>> groupWordsByAlphabet(String[] words) {
        Map<Character, ArrayList<String>> groupWords = new TreeMap<Character, ArrayList<String>>();
        ArrayList<String> list = new ArrayList<>(Arrays.asList(words));
        for (int i = 0; i < words.length; i++) {
            char[] letters = words[i].toCharArray();
            if (!groupWords.containsKey(letters[0])) {
                ArrayList<String> temporaryList = new ArrayList<String>();
                for (String str : list) {
                    char[] temporaryArray = str.toCharArray();
                    if (letters[0] == temporaryArray[0]) {
                        temporaryList.add(str);
                    }
                }
                groupWords.put(letters[0], temporaryList);
            }
        }
        return groupWords;
    }
}
