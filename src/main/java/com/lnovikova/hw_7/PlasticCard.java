package main.java.com.lnovikova.hw_7;

public class PlasticCard {

    private double balance;
    public PlasticCard(double initialBalance)
    {
        balance=initialBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
