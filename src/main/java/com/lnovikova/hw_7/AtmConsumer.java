package main.java.com.lnovikova.hw_7;

public class AtmConsumer implements Runnable {

    private PlasticCard card;


    public PlasticCard getCard() {
        return card;
    }

    public void setCard(PlasticCard card) {
        this.card = card;
    }


    @Override
    public void run() {


        while (card.getBalance() > 0 && card.getBalance() < 1000) {
            int deposit = (5 + (int) (Math.random() * 10));
            card.setBalance(card.getBalance() - deposit);
            System.out.println("Card balance 1: " + card.getBalance()+" Thread: "+Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                if(card.getBalance()<0)
                    System.out.print("Баланс отрицательный");
                else if(card.getBalance()>1000)
                    System.out.print("Баланс больше 1000");
            }

        }

    }
}

