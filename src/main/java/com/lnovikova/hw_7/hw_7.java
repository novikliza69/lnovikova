package main.java.com.lnovikova.hw_7;

import java.util.concurrent.*;

public class hw_7 {

    public static void main(String[] args) {
        PlasticCard card = new PlasticCard(500);
        AtmConsumer consumer1 = new AtmConsumer();
        AtmConsumer consumer2 = new AtmConsumer();
        AtmProducer producer1 = new AtmProducer();
        AtmProducer producer2 = new AtmProducer();
        consumer1.setCard(card);
        consumer2.setCard(card);
        producer1.setCard(card);
        producer2.setCard(card);
        Thread consumerThread1 = new Thread(consumer1);
        Thread consumerThread2 = new Thread(consumer2);
        Thread producerThread1 = new Thread(producer1);
        Thread producerThread2 = new Thread(producer2);
        ExecutorService executor;
        executor = Executors.newFixedThreadPool(4);

        executor.execute(consumerThread1);
        executor.execute(consumerThread2);
        executor.execute(producerThread1);
        executor.execute(producerThread2);
        try {

                Thread.sleep(500);

        }catch (InterruptedException e){

        }
        executor.shutdown();


    }
}
