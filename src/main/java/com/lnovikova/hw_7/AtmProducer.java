package main.java.com.lnovikova.hw_7;

public class AtmProducer implements Runnable {
    private PlasticCard card;

    public PlasticCard getCard() {
        return card;
    }

    public void setCard(PlasticCard card) {
        this.card = card;
    }


    @Override
    public void run() {
        while (card.getBalance() > 0 && card.getBalance() < 1000) {
            int deposit = 5 + (int) (Math.random() * 10);
            card.setBalance(card.getBalance() + deposit);
            System.out.println("Card balance: " + getCard().getBalance()+" Thread: "+Thread.currentThread().getName());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {

            }

        }

    }

}

