package main.java.com.lnovikova.hw_4;

public class DebitCard extends Card {
    public DebitCard(String cardOwner) {
        super(cardOwner);
    }

    public DebitCard(String cardOwner, double initialBalance) {
        super(cardOwner, initialBalance);
    }

    @Override
    public double withdraw(double deposit) {
        if ((super.getCardBalance() - deposit) >= 0) {
            return super.withdraw(deposit);
        } else {
            return super.getCardBalance();
        }
    }
}
