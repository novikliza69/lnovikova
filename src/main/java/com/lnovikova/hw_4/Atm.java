package main.java.com.lnovikova.hw_4;

public class Atm {

    private Card card;
    public double replenishBalance(double deposit) {
        card.replenishBalance(deposit);
        return card.getCardBalance();
    }

    public double withdraw( double deposit) {
        card.withdraw(deposit);
        return card.getCardBalance();
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
