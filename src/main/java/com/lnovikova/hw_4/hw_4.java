package main.java.com.lnovikova.hw_4;

public class hw_4 {
    public static void main(String[] args) {
        Card creditCard = new CreditCard("Novikova");
        Card debitCard = new DebitCard("Tsikavenko", 12.3);
        Atm atm = new Atm();
        atm.setCard(creditCard);
        atm.replenishBalance( 7);
        atm.setCard(debitCard);
        atm.withdraw(5);
        System.out.print("Баланс дебетовой карты :" + debitCard.getCardBalance());
        System.out.print("\nБаланс кредитной карты :" + creditCard.getCardBalance());
        System.out.print("\nБаланс дебетовой карты в евро: " + debitCard.getBalanceInForeignCurrency(0.43));
    }
}
