package main.java.com.lnovikova.hw_4;

public class CreditCard extends Card {

    public CreditCard(String cardOwner) {
        super(cardOwner);
    }

    public CreditCard(String cardOwner, double initialBalance) {
        super(cardOwner, initialBalance);
    }

    @Override
    public double withdraw(double deposit) {
        return super.withdraw(deposit);
    }
}
