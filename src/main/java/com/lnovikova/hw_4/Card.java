package main.java.com.lnovikova.hw_4;

public class Card {
    String cardOwner;
    private double cardBalance;

    public Card(String cardOwner) {
        this.cardOwner = cardOwner;
        cardBalance = 0.0;
    }

    public Card(String cardOwner, double initialBalance) {
        this.cardOwner = cardOwner;
        cardBalance = initialBalance;
    }

    public double getCardBalance() {
        return cardBalance;
    }

    public double replenishBalance(double deposit) {
        setCardBalance(getCardBalance() + deposit);
        return cardBalance;
    }

    public double withdraw(double deposit) {
        setCardBalance(getCardBalance() - deposit);
        return cardBalance;
    }

    public double getBalanceInForeignCurrency(double conversionCourse) {
        return cardBalance *= conversionCourse;
    }

    public void setCardBalance(double cardBalance) {
        this.cardBalance = cardBalance;
    }
}
