package main;

import java.util.InputMismatchException;

public class hw2_2 {

    public static void main(String[] args) {
        try {
            int algorithmId = Integer.parseInt(args[0]);
            int loopType = Integer.parseInt(args[1]);
            int n = Integer.parseInt(args[2]);
            String result = "";
            if (algorithmId == 1) {
                print(fibonaci(n, loopType));
            } else if (algorithmId == 2) {
                result = Integer.toString(factorial(n, loopType));
            } else {
                System.out.print("algorithmId Error");
            }
            System.out.print(result);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (ArithmeticException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (NumberFormatException ex) {
            System.out.print("Error:" + ex.getMessage());
        }

    }

    public static int[] fibonaci(int n, int loopType) {
        int[] fibonaciNumbers = new int[n];
        fibonaciNumbers[0] = 0;
        int i = 2;
        if (n > 1) {
            fibonaciNumbers[1] = 1;
            if (loopType == 1) {
                while (i < n) {
                    fibonaciNumbers[i] = fibonaciNumbers[i - 1] + fibonaciNumbers[i - 2];
                    i++;
                }
            } else if (loopType == 2) {
                do {
                    fibonaciNumbers[i] = fibonaciNumbers[i - 1] + fibonaciNumbers[i - 2];
                    i++;
                } while (i < n);
            } else if (loopType == 3) {
                for (i = 2; i < n; i++) {
                    fibonaciNumbers[i] = fibonaciNumbers[i - 1] + fibonaciNumbers[i - 2];
                }

            } else {
                System.out.print("loopType Error");
            }
        }
        return fibonaciNumbers;
    }

    public static int factorial(int n, int loopType) {
        int resultFactorial = 1;
        int i = 1;
        if (loopType == 1) {
            while (i <= n) {
                resultFactorial *= i;
                i++;
            }
        } else if (loopType == 2) {
            do {
                resultFactorial *= i;
                i++;
            } while (i <= n);
        } else if (loopType == 3) {
            for (i = 1; i <= n; ++i)
                resultFactorial *= i;
        } else {
            System.out.print("Error");
        }
        return resultFactorial;
    }

    public static void print(int[] array) {
        for (int i = 0; i < array.length; i++)
            System.out.print(" " + array[i]);
    }
}





