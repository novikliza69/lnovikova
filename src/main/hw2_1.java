package main;

import java.util.InputMismatchException;

public class hw2_1 {

    static double function(int a, int p, double m1, double m2) {
        return (4 * Math.pow(Math.PI, 2) * (Math.pow(a, 3) / (Math.pow(p, 2) * (m1 + m2))));
    }

    public static void main(String args[]) {
        try {
            int a = Integer.parseInt(args[0]);
            int p = Integer.parseInt(args[1]);
            double m1 = Double.parseDouble(args[2]);
            double m2 = Double.parseDouble(args[3]);
            double G = function(a, p, m1, m2);
            System.out.println("" + G + "");
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (ArithmeticException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (InputMismatchException ex) {
            System.out.print("Error:" + ex.getMessage());
        } catch (NumberFormatException ex) {
            System.out.print("Error:" + ex.getMessage());
        }
    }
}
