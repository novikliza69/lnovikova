package main.java.com.lnovikova.hw_4;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreditCardTest {

    static Card card ;
    int delta=1;

    @BeforeClass
    public static  void setUp(){
        card =new CreditCard("Novikova", 100.87);
    }
    @Test
    public void testWithdraw() {
        double expected = 100.97;
        double actual = card.withdraw(-0.1);
        assertEquals(expected, actual, delta);
    }
}