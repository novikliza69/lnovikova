package main.java.com.lnovikova.hw_4;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class DebitCardTest {
    static Card card ;
    int delta=1;

    @BeforeClass
    public static  void setUp(){
        card =new DebitCard("Novikova", 30);
    }
    @Test
    public void testWithdraw() {
        double expected = 10;
        double actual = card.withdraw(20);
        assertEquals(expected, actual, delta);
    }
}