package main.java.com.lnovikova.hw_4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AtmTest {
    Atm atm = new Atm();
    Card card = new DebitCard("Novikova", 12.3);
    int delta=1;

    @Test
    public void testReplenishBalance() {
        atm.setCard(card);
        double expected = 17.3;
        double actual = atm.replenishBalance( 5);
        assertEquals(expected, actual, delta);
    }

    @Test
    public void testWithdraw() {
        atm.setCard(card);

        double expected = 7.3;
        double actual = atm.withdraw(5);
        assertEquals(expected, actual, delta);
    }
}