package main.java.com.lnovikova.hw_4;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {
    Card card = new Card("Novikova", 10);
    int delta=1;

    @Test
    public void testGetCardBalance() {
        double expected = 10;
        double actual = card.getCardBalance();
        assertEquals(expected, actual,delta);
    }

    @Test
    public void testReplenishBalance() {
        double expected = 30;
        double actual = card.replenishBalance(20);
        assertEquals(expected, actual,delta);
    }

    @Test
    public void testWithdraw() {
        double expected = 0.2;
        double actual = card.withdraw(9.8);
        assertEquals(expected, actual,delta);
    }

    @Test
    public void testGetBalanceInForeignCurrency() {
        double expected = 5;
        double actual = card.getBalanceInForeignCurrency(0.5);
        assertEquals(expected, actual,delta);
    }
}