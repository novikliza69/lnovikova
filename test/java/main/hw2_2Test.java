package main;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class hw2_2Test {
    @org.junit.Test
    public void testFibonaci() {
        int[] expected = {0, 1, 1, 2, 3};
        int[] actual = hw2_2.fibonaci(5, 3);
        assertArrayEquals(actual, expected);
    }

    @org.junit.Test
    public void factorial() {
        int expected = 120;
        int actual = hw2_2.factorial(5, 3);
        assertEquals(actual, expected, 1);
    }
}