package main;

import static org.junit.Assert.assertEquals;

public class hw2_1Test {
    @org.junit.Test
    public void testFunction() {
        double expected = 19.739208802178716;
        double actual = hw2_1.function(1, 1, 1, 1);
        assertEquals(expected, actual, 1);
    }
}